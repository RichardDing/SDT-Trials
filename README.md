# This repository provides the SDT example trial images

## Please read this instruction before you update your SDT Trial images


### How to app stimuli to SDT app:

1.Download this folder to you computer

2. Connect you iPad to a mac computer

3. Open iTunes

4. Click your iPad icon

5. Click File Sharing on left side menu

6. Find the SDT app and click the app icon

7. Drag and drop the “AdultTestTrials” and “ChildTestTrials” folders from this project to S.D.T Documents area

7. Click replace to replace the existing two folders.

8. If you need to update trial images, please use the same folder name, same file naming convention and same picture resolution.

### How to configure practice trials of SDT app:

1. Connect you iPad to a mac computer

2. Open iTunes

3. Click your iPad icon

4. Click File Sharing on left side menu

5. Find the SDT app and click the app icon

6. Find the configure.plist in the S.D.T Documents area

7. Drag and drop the file to your Desktop

8. Open configure.plist file from you Desktop using any text file open software

9. Find the line “<key>adult_practice_list</key>”

10. The next line is the adult practice trial list “<string>plain gene skit robe bin plin gean skti vobe bni</string>”

11. You can edit the text between the two <string>, and make sure the even number text is correct trial and odd number text is incorrect trials. 

12. You could update the child practice trail list same way by finding the line “<key>child_practice_list</key>”

13. Save configure.plist file. 

14 Drag and drop the updated configure.plist file back to the iTunes S.D.T Documents area. 

15. Click replace to replace the existing configure.plist file

